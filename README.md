# Hamster Blogging platform

Simple blog created using React.js & Node.js

### Prerequisites

Make sure you have these installed on your machine

* [Node.js](https://nodejs.org/en/download/)
* [MongoDB](https://www.mongodb.com)
* **npm** This comes with Node.js, but make sure you check if you have it anyway

### Installing packages

Install backend packages

```
cd server/
npm i
```

Install frontend packages

```
cd client/
npm i
```

### Running the app

To run the app (dev. mode)

```
cd server
node app.js

cd client
npm start
```
### Logging in and Registration

 
* Goto http://localhost:8080/ in the browser window.

* Press the Register link.

* Enter details asked and press Register button.

* Login with the details on the login screen.

* Enter the blog post in the subsequent form.
 
```
N.B. - If you have MongoDB running use ForumAdd branch which has code to show & like posts
```

## Built With

* [Node.js](https://nodejs.org) - The backend framework used
* [Express.js](https://github.com/expressjs/express) - Node.js framework used
* [React.js](https://github.com/facebook/react) - The frontend framework used
* [MongoDB](https://www.mongodb.com/) - Database platform used



